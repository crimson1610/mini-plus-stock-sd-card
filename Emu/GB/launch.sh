#!/bin/sh
echo $0 $*

#add old libtmenu.so PATH
export LD_LIBRARY_PATH=./:./lib/:$LD_LIBRARY_PATH

progdir=`dirname "$0"`
homedir=`dirname "$1"`
cd $progdir

./cpufreq.sh

HOME=$homedir $progdir/gambatte "$1"
